module 0xacab.org/leap/ooni-exporter

go 1.15

require (
	github.com/cretz/bine v0.1.0
	github.com/golang/protobuf v1.4.3
	github.com/ooni/probe-engine v0.22.0
	github.com/pborman/getopt/v2 v2.1.0
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.9.0
	github.com/stretchr/testify v1.6.1
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b
	google.golang.org/grpc v1.27.0
)
