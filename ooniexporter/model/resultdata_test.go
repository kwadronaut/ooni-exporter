package model_test

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"testing"

	model "0xacab.org/leap/ooni-exporter/ooniexporter/model"
	"github.com/ooni/probe-engine/experiment/riseupvpn"
	probe "github.com/ooni/probe-engine/model"
)

func TestNewResultData(t *testing.T) {
	result := model.NewResultData()
	if result.FailingGatewayConnections == nil {
		t.Fatal("FailingGatewayConnections should not be null")
	}

	defer func() {
		if r := recover(); r != nil {
			t.Fatal("ResultData incorrectly initialized")
		}
	}()
	result.AddMeasurement(probe.Measurement{})
}

func TestAddMeasurement(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Fatal("AddMeasurement failed unexpectedly")
		}
	}()

	measurement := getMeasurementFailingGWS1(t)
	result := model.NewResultData()
	result.AddMeasurement(*measurement)
}

func TestCountryForASN(t *testing.T) {
	measurement := getMeasurementSuccess(t)
	measurement2 := getMeasurementSuccessASN2(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)

	cc := result.CountryForASN("AS3210")
	if cc != "DE" {
		t.Fatal("Unexpected country for ASN found. Expected: DE - actual: " + cc)
	}
	cc = result.CountryForASN("AS3209")
	if cc != "DE" {
		t.Fatal("Unexpected country for ASN found. Expected: DE - actual: " + cc)
	}
	cc = result.CountryForASN("AS3289")
	if cc != "n/a" {
		t.Fatal("A country for ASN was unexpectedly found. Expected: n/a - actual: " + cc)
	}

}

func TestSuccessfulTestsByNetwork_multipleNetworks(t *testing.T) {
	measurement := getMeasurementFailingGWS2(t)
	measurement2 := getMeasurementSuccess(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	resultMap := result.SuccessfulTestsByNetwork()

	checkLength(t, resultMap, 2)
	checkResult(t, resultMap, "AS3210", 0)
	checkResult(t, resultMap, "AS3209", 1)
}

func TestSuccessfulTestsByNetwork_multipleTestsOneNetwork(t *testing.T) {
	measurement := getMeasurementFailingGWS1(t)
	measurement2 := getMeasurementSuccess(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement2)
	resultMap := result.SuccessfulTestsByNetwork()

	checkLength(t, resultMap, 1)
	checkResult(t, resultMap, "AS3209", 2)
}

func TestFailingTestsByNetwork_multipleNetworks(t *testing.T) {
	measurement := getMeasurementFailingGWS1(t)
	measurement2 := getMeasurementSuccess(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement2)
	resultMap := result.FailingTestsByNetwork()

	checkLength(t, resultMap, 1)
	checkResult(t, resultMap, "AS3209", 1)
}

func TestSuccessfulTestsByCountry_multipleNetworksOneCountry(t *testing.T) {
	measurement := getMeasurementSuccess(t)
	measurement2 := getMeasurementSuccessASN2(t)
	measurement3 := getMeasurementFailingAPI(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	resultMap := result.SuccessfulTestsByCountry()

	checkLength(t, resultMap, 1)
	checkResult(t, resultMap, "DE", 2)
}

func TestFailingTestsByCountry_multipleNetworksOneCountry(t *testing.T) {
	measurement := getMeasurementFailingGWS1(t)
	measurement2 := getMeasurementFailingGWS2(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	resultMap := result.FailingTestsByCountry()

	checkLength(t, resultMap, 1)
	checkResult(t, resultMap, "DE", 2)
}

func TestSuccessfulTestsByCountry_multipleCountries(t *testing.T) {
	measurement := getMeasurementSuccess(t)
	measurement2 := getMeasurementFailingApiIR(t)
	measurement3 := getMeasurementFailingAPI(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	resultMap := result.SuccessfulTestsByCountry()

	checkLength(t, resultMap, 2)
	checkResult(t, resultMap, "DE", 1)
	checkResult(t, resultMap, "IR", 0)
}

func TestFailingTestsByCountry_multipleCountries(t *testing.T) {
	measurement := getMeasurementSuccess(t)
	measurement2 := getMeasurementFailingApiIR(t)
	measurement3 := getMeasurementFailingAPI(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	resultMap := result.FailingTestsByCountry()

	checkLength(t, resultMap, 2)
	checkResult(t, resultMap, "DE", 1)
	checkResult(t, resultMap, "IR", 1)
}

func TestFailingAPIByNetwork(t *testing.T) {
	measurement := getMeasurementSuccess(t)
	measurement2 := getMeasurementFailingApiIR(t)
	measurement3 := getMeasurementFailingAPI(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	resultMap := result.FailingAPIByNetwork()

	checkLength(t, resultMap, 2)
	checkResult(t, resultMap, "AS197207", 1)
	checkResult(t, resultMap, "AS3209", 1)
}

func TestSuccessfulAPIByNetwork(t *testing.T) {
	measurement := getMeasurementSuccess(t)
	measurement2 := getMeasurementFailingApiIR(t)
	measurement3 := getMeasurementFailingAPI(t)
	measurement4 := getMeasurementSuccessASN2(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	result.AddMeasurement(*measurement4)
	resultMap := result.SuccessfulAPIByNetwork()

	checkLength(t, resultMap, 3)
	checkResult(t, resultMap, "AS197207", 0)
	checkResult(t, resultMap, "AS3209", 1)
	checkResult(t, resultMap, "AS3210", 1)
}

func TestSuccessfulGatewayStatusByNetwork(t *testing.T) {
	measurement := getMeasurementSuccess(t)
	measurement2 := getMeasurementFailingApiIR(t)
	measurement3 := getMeasurementFailingAPI(t)
	measurement4 := getMeasurementSuccessASN2(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	result.AddMeasurement(*measurement4)
	resultMap := result.SuccessfulGatewayStatusByNetwork()

	checkLength(t, resultMap, 3)
	checkResult(t, resultMap, "AS197207", 0)
	checkResult(t, resultMap, "AS3209", 1)
	checkResult(t, resultMap, "AS3210", 1)
}

func TestUnknownGatewayStatusByNetwork(t *testing.T) {
	measurement := getMeasurementSuccess(t)
	measurement2 := getMeasurementFailingApiIR(t)
	measurement3 := getMeasurementFailingAPI(t)
	measurement4 := getMeasurementSuccessASN2(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	result.AddMeasurement(*measurement4)
	resultMap := result.UnknownGatewayStatusByNetwork()

	checkLength(t, resultMap, 3)
	checkResult(t, resultMap, "AS197207", 1)
	checkResult(t, resultMap, "AS3209", 1)
	checkResult(t, resultMap, "AS3210", 0)
}

func TestFailingGatewayStatusByNetwork(t *testing.T) {
	measurement := getMeasurementSuccess(t)
	measurement2 := getMeasurementFailingApiIR(t)
	measurement3 := getMeasurementFailingGWS2(t)
	measurement4 := getMeasurementFailingGWS1(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	result.AddMeasurement(*measurement4)
	resultMap := result.FailingGatewayStatusByNetwork()

	checkLength(t, resultMap, 3)
	checkResult(t, resultMap, "AS197207", 0)
	checkResult(t, resultMap, "AS3209", 1)
	checkResult(t, resultMap, "AS3210", 1)
}

func TestFailingGatewayStatusByNetwork_AddMeasurementTwice_twoOccurences(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Fatal("AddMeasurement failed unexpectedly")
		}
	}()

	measurement := getMeasurementFailingGWS1(t)
	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement)
	resultMap := result.FailingGatewayStatusByNetwork()
	checkLength(t, resultMap, 1)
	checkResult(t, resultMap, "AS3209", 2)
}

func TestFailingGatewayConnectionsByIP(t *testing.T) {
	measurement := getMeasurementSuccess(t)
	measurement2 := getMeasurementFailingApiIR(t)
	measurement3 := getMeasurementFailingGWS2(t)
	measurement4 := getMeasurementFailingGWS1(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	result.AddMeasurement(*measurement4)
	resultMap := result.FailingGatewayConnections

	expectedResults198_252_153_28 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "198.252.153.28",
				Port:          23042,
				TransportType: "obfs4",
			},
			ProbeASN:   "AS3209",
			ProbeCC:    "DE",
			Occurences: 1,
		},
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "198.252.153.28",
				Port:          23042,
				TransportType: "obfs4",
			},
			ProbeASN:   "AS3210",
			ProbeCC:    "DE",
			Occurences: 1,
		},
	}

	expectedResults37_218_241_106 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "37.218.241.106",
				Port:          443,
				TransportType: "openvpn",
			},
			ProbeASN:   "AS3209",
			ProbeCC:    "DE",
			Occurences: 1,
		},
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "37.218.241.106",
				Port:          443,
				TransportType: "openvpn",
			},
			ProbeASN:   "AS3210",
			ProbeCC:    "DE",
			Occurences: 1,
		},
	}

	expectedResults212_83_182_127 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "212.83.182.127",
				Port:          443,
				TransportType: "openvpn",
			},
			ProbeASN:   "AS3209",
			ProbeCC:    "DE",
			Occurences: 1,
		},
	}

	checkGatewayMapLength(t, resultMap, 3)
	checkResultGateways(t, resultMap, "198.252.153.28", expectedResults198_252_153_28)
	checkResultGateways(t, resultMap, "37.218.241.106", expectedResults37_218_241_106)
	checkResultGateways(t, resultMap, "212.83.182.127", expectedResults212_83_182_127)
}

func TestFailingGatewayConnections_MultipleOccurences(t *testing.T) {
	measurement := getMeasurementFailingGWS2(t)
	measurement2 := getMeasurementFailingGWS1(t)
	measurement3 := getMeasurementFailingGWS3(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	resultMap := result.FailingGatewayConnections

	expectedResults198_252_153_28 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "198.252.153.28",
				Port:          23042,
				TransportType: "obfs4",
			},
			ProbeASN:   "AS3209",
			ProbeCC:    "DE",
			Occurences: 2,
		},
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "198.252.153.28",
				Port:          443,
				TransportType: "openvpn",
			},
			ProbeASN:   "AS3209",
			ProbeCC:    "DE",
			Occurences: 1,
		},
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "198.252.153.28",
				Port:          23042,
				TransportType: "obfs4",
			},
			ProbeASN:   "AS3210",
			ProbeCC:    "DE",
			Occurences: 1,
		},
	}

	checkResultGateways(t, resultMap, "198.252.153.28", expectedResults198_252_153_28)
}

func TestFailingGatewayConnectionsByIPAndNetwork(t *testing.T) {
	measurement := getMeasurementFailingGWS2(t)
	measurement2 := getMeasurementFailingGWS1(t)
	measurement3 := getMeasurementFailingGWS3(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	resultMap := result.FailingGatewayConnectionsByIPAndNetwork()

	expectedResults198_252_153_28 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "198.252.153.28",
				Port:          -1,
				TransportType: "n/a",
			},
			ProbeASN:   "AS3209",
			ProbeCC:    "DE",
			Occurences: 3,
		},
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "198.252.153.28",
				Port:          -1,
				TransportType: "n/a",
			},
			ProbeASN:   "AS3210",
			ProbeCC:    "DE",
			Occurences: 1,
		},
	}

	expectedResults37_218_241_106 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "37.218.241.106",
				Port:          -1,
				TransportType: "n/a",
			},
			ProbeASN:   "AS3209",
			ProbeCC:    "DE",
			Occurences: 1,
		},
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "37.218.241.106",
				Port:          -1,
				TransportType: "n/a",
			},
			ProbeASN:   "AS3210",
			ProbeCC:    "DE",
			Occurences: 1,
		},
	}

	expectedResults212_83_182_127 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "212.83.182.127",
				Port:          -1,
				TransportType: "n/a",
			},
			ProbeASN:   "AS3209",
			ProbeCC:    "DE",
			Occurences: 1,
		},
	}

	checkGatewayMapLength(t, resultMap, 3)
	checkResultGateways(t, resultMap, "198.252.153.28", expectedResults198_252_153_28)
	checkResultGateways(t, resultMap, "37.218.241.106", expectedResults37_218_241_106)
	checkResultGateways(t, resultMap, "212.83.182.127", expectedResults212_83_182_127)
}

func TestFailingGatewayConnectionsByIPAndCountry(t *testing.T) {
	measurement := getMeasurementFailingGWS2(t)
	measurement2 := getMeasurementFailingGWS1(t)
	measurement3 := getMeasurementFailingGWS3(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	resultMap := result.FailingGatewayConnectionsByIPAndCountry()

	expectedResults198_252_153_28 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "198.252.153.28",
				Port:          -1,
				TransportType: "n/a",
			},
			ProbeASN:   "n/a",
			ProbeCC:    "DE",
			Occurences: 4,
		},
	}

	expectedResults37_218_241_106 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "37.218.241.106",
				Port:          -1,
				TransportType: "n/a",
			},
			ProbeASN:   "n/a",
			ProbeCC:    "DE",
			Occurences: 2,
		},
	}

	expectedResults212_83_182_127 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "212.83.182.127",
				Port:          -1,
				TransportType: "n/a",
			},
			ProbeASN:   "n/a",
			ProbeCC:    "DE",
			Occurences: 1,
		},
	}

	checkGatewayMapLength(t, resultMap, 3)
	checkResultGateways(t, resultMap, "198.252.153.28", expectedResults198_252_153_28)
	checkResultGateways(t, resultMap, "37.218.241.106", expectedResults37_218_241_106)
	checkResultGateways(t, resultMap, "212.83.182.127", expectedResults212_83_182_127)
}

func TestFailingGatewayConnectionsByIPAndTransportAndCountry(t *testing.T) {
	measurement := getMeasurementFailingGWS2(t)
	measurement2 := getMeasurementFailingGWS1(t)
	measurement3 := getMeasurementFailingGWS3(t)

	result := model.NewResultData()
	result.AddMeasurement(*measurement)
	result.AddMeasurement(*measurement2)
	result.AddMeasurement(*measurement3)
	resultMap := result.FailingGatewayConnectionsByIPAndTransportAndCountry()

	expectedResults198_252_153_28 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "198.252.153.28",
				Port:          -1,
				TransportType: "obfs4",
			},
			ProbeASN:   "n/a",
			ProbeCC:    "DE",
			Occurences: 3,
		},
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "198.252.153.28",
				Port:          -1,
				TransportType: "openvpn",
			},
			ProbeASN:   "n/a",
			ProbeCC:    "DE",
			Occurences: 1,
		},
	}

	expectedResults37_218_241_106 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "37.218.241.106",
				Port:          -1,
				TransportType: "openvpn",
			},
			ProbeASN:   "n/a",
			ProbeCC:    "DE",
			Occurences: 2,
		},
	}

	expectedResults212_83_182_127 := []model.FailingGatewayConnection{
		{
			GatewayConnection: riseupvpn.GatewayConnection{
				IP:            "212.83.182.127",
				Port:          -1,
				TransportType: "openvpn",
			},
			ProbeASN:   "n/a",
			ProbeCC:    "DE",
			Occurences: 1,
		},
	}

	checkGatewayMapLength(t, resultMap, 3)
	checkResultGateways(t, resultMap, "198.252.153.28", expectedResults198_252_153_28)
	checkResultGateways(t, resultMap, "37.218.241.106", expectedResults37_218_241_106)
	checkResultGateways(t, resultMap, "212.83.182.127", expectedResults212_83_182_127)
}

func checkResultGateways(t *testing.T, resultMap map[string][]model.FailingGatewayConnection, key string, expectedGatewayConnections []model.FailingGatewayConnection) {
	gateways, ok := resultMap[key]
	if !ok {
		t.Fatal("expected key " + key + " not found")
	}

	if len(gateways) != len(expectedGatewayConnections) {
		t.Fatal("expected number of gateways in " + key + ": " + fmt.Sprint(len(expectedGatewayConnections)) + " - actual: " + fmt.Sprint(len(gateways)))
	}

	for _, expectedGateway := range expectedGatewayConnections {
		found := false
		for _, gateway := range gateways {
			if gateway.Equals(expectedGateway) {
				if gateway.Occurences != expectedGateway.Occurences {
					t.Fatal("unexpected number of failing gateway connection occurences. Expected: " + fmt.Sprint(expectedGateway.Occurences) + " actual: " + fmt.Sprint(gateway.Occurences))
				}
				found = true
			}
		}
		if !found {
			t.Fatal("expected failing gateway connection not found: " + fmt.Sprint(expectedGateway) + " in \n" + fmt.Sprint(gateways))
		}
	}
}

func checkGatewayMapLength(t *testing.T, resultMap map[string][]model.FailingGatewayConnection, expectedLength int) {
	if len(resultMap) != expectedLength {
		t.Fatal("unexpected number of results. expected: " + fmt.Sprint(expectedLength) + " actual: " + fmt.Sprint(len(resultMap)))
	}
}
func checkLength(t *testing.T, resultMap map[string]int, expectedLength int) {
	if len(resultMap) != expectedLength {
		t.Fatal("unexpected number of results. expected: " + fmt.Sprint(expectedLength) + " actual: " + fmt.Sprint(len(resultMap)))
	}
}

func checkResult(t *testing.T, resultMap map[string]int, key string, expectedOccurences int) {
	occurence, ok := resultMap[key]
	if !ok {
		t.Fatal("expected key " + key + " not found")
	}
	if occurence != expectedOccurences {
		t.Fatal("expected number of occurences in " + key + ": " + fmt.Sprint(expectedOccurences) + " - actual: " + fmt.Sprint(occurence))
	}
}

func getMeasurement(t *testing.T, path string) *probe.Measurement {
	testfile, err := ioutil.ReadFile(path)
	if err != nil {
		err = errors.New("getMeasurement could not open test file: " + fmt.Sprint(err))
		t.Fatal(err)
	}

	var measurement probe.Measurement
	if err := json.Unmarshal(testfile, &measurement); err != nil {
		err = errors.New("getMeasurement could not parse test file: " + fmt.Sprint(err))
		t.Fatal(err)
	}
	return &measurement
}

func getMeasurementSuccessASN2(t *testing.T) *probe.Measurement {
	return getMeasurement(t, "../testdata/result_de_success2.json")
}
func getMeasurementSuccess(t *testing.T) *probe.Measurement {
	return getMeasurement(t, "../testdata/result_de_success.json")
}

func getMeasurementFailingGWS1(t *testing.T) *probe.Measurement {
	return getMeasurement(t, "../testdata/result_de_failing_gws1.json")
}

func getMeasurementFailingGWS2(t *testing.T) *probe.Measurement {
	return getMeasurement(t, "../testdata/result_de_failing_gws2.json")
}

func getMeasurementFailingGWS3(t *testing.T) *probe.Measurement {
	return getMeasurement(t, "../testdata/result_de_failing_gws3.json")
}

func getMeasurementFailingAPI(t *testing.T) *probe.Measurement {
	return getMeasurement(t, "../testdata/result_de_failing_api.json")
}

func getMeasurementFailingApiIR(t *testing.T) *probe.Measurement {
	return getMeasurement(t, "../testdata/result_ir_failing_api.json")
}
