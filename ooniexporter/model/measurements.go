package model

// Measurements partial representation of /api/v1/measurements json response body
type Measurements struct {
	Metadata MetaData `json:"metadata"`
	Results  []Result `json:"results"`
}

// MetaData contains part of the metadata json
type MetaData struct {
	Count   float64 `json:"count"`
	NextURL *string `json:"next_url"`
}

// Result contains part  of the result json
type Result struct {
	Anomaly              bool   `json:"anomaly"`
	Confirmed            bool   `json:"confirmed"`
	Failure              bool   `json:"failure"`
	MeasurementStartTime string `json:"measurement_start_time"`
	MeasurementURL       string `json:"measurement_url"`
	ProbeASN             string `json:"probe_asn"`
	ProbeCC              string `json:"probe_cc"`
}
