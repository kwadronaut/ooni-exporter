package model

import (
	"crypto"
	"fmt"

	"github.com/ooni/probe-engine/experiment/riseupvpn"
)

// TestResult contains the main details of an experiment
type TestResult struct {
	EngineName      string
	EngineVersion   string
	ProbeCC         string
	ResolverASN     string
	ProbeASN        string
	ReportID        string
	APIStatus       bool
	GatewayStatus   bool
	APIFailure      *string
	CACertStatus    bool
	FailingGateways []riseupvpn.GatewayConnection
}

// Hash generates a hash over the country code and the experiment stati of a TestResult
func (t *TestResult) Hash() string {
	digester := crypto.MD5.New()

	fmt.Fprint(digester, t.ProbeCC)
	fmt.Fprint(digester, t.APIStatus)
	fmt.Fprint(digester, t.GatewayStatus)
	fmt.Fprint(digester, &t.APIFailure)
	fmt.Fprint(digester, t.CACertStatus)

	return string(digester.Sum(nil))
}
