package model

// ResponseError contains either a HTTP error code or an error
type ResponseError struct {
	Err        error
	HTTPStatus *int
}

// ErrorResult contains either a HTTP error code or an error
type ErrorResult struct {
	Errors []ResponseError
}

// NewErrorResult instanciates a new ErrorResult struct
func NewErrorResult() *ErrorResult {
	result := new(ErrorResult)
	result.Errors = []ResponseError{}
	return result
}

// GetErrorStatus returns the number of occurances of HTTP status codes
// defined by the parameter statusCode
func (result *ErrorResult) GetErrorStatus(statusCode int) int {
	counter := 0
	for _, err := range result.Errors {
		if err.HTTPStatus != nil && *err.HTTPStatus == statusCode {
			counter++
		}
	}
	return counter
}

// GetAllErrors returns the number of all registered errors, not including http status codes
func (result *ErrorResult) GetAllErrors() int {
	counter := 0
	for _, err := range result.Errors {
		if err.Err != nil {
			counter++
		}
	}
	return counter
}

// GetAllHTTPStatusCodes returns the number of all registered HTTP status codes
func (result *ErrorResult) GetAllHTTPStatusCodes() int {
	counter := 0
	for _, err := range result.Errors {
		if err.HTTPStatus != nil {
			counter++
		}
	}
	return counter
}

// AddResponseError adds a non nil ResponseError struct to ErrorResult
func (result *ErrorResult) AddResponseError(responseError *ResponseError) {
	if responseError != nil {
		result.Errors = append(result.Errors, *responseError)
	}
}

// HasErrors returns true if ResponseErrors have been added
func (result *ErrorResult) HasErrors() bool {
	return len(result.Errors) > 0
}
